Aplicação criada para execução de backup de routerOS


exemplo de utilização:

# executando backup para todos os alvos do arquivo /opt/backup/audit/client_info
/opt/backup/scripts/start_backup all  

# executando backup para um alvo especifico.
/opt/backup/scripts/start_backup nome_do_ativo

Aplicação licenciada, entrar em contato com o criador para gerar licença.
E-mail: bruno.cavalcanti@mylabs.dev.br

diretorio padrão da aplicação: /opt/backup

Arquivo /opt/backup/audit/client_info contem as informações de acesso dos alvos.
Arquivo /opt/backup/ftp_server contem as informações de acesso ao servidor FTP.
Arquivo /opt/backup/conf/backup_config contem as configurações de pool e sleep
